﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Polycrime;
using UnityEngine.SceneManagement;

public class PlayAgain : MonoBehaviour {
    public Canvas playAgainCanvas;
    public bool togle = false;
    private GestureDemoEvent gestureObj = new GestureDemoEvent();
    private PropelRigidbody propelObj = new PropelRigidbody();
    public GameObject gestureCanvas;
    public GameObject jump, up, down, left, right;
   
    public void playAgain()
    {
        gestureObj.monsterBlastCnt = 0;
        gestureObj.wrongAttemptCnt = 0;
        propelObj.creatureHitCnt = 0;
        propelObj.creatureEscapeCnt = 0;

        gestureCanvas.SetActive(false);
        AudioSource[] allAudioSources;

        allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in allAudioSources)
        {
            if (audioS.clip.name == "Kids Cheering-SoundBible.com-681813822")
                audioS.Stop();
        }

        jump.SetActive(true);
        up.SetActive(true);
        down.SetActive(true);
        left.SetActive(true);
        right.SetActive(true);
        playAgainCanvas = GameObject.FindGameObjectWithTag("canvas_playAgain").GetComponent<Canvas>();
        playAgainCanvas.enabled = false;
        GameObject.FindGameObjectWithTag("canvas_playAgainWin").GetComponent<Canvas>().enabled = false;

        Time.timeScale = 1;
        GameObject.FindGameObjectWithTag("img_creatureProgress").GetComponent<Image>().enabled = true;
        GameObject.FindGameObjectWithTag("creature").GetComponent <Renderer>().material.mainTexture = (Texture)Resources.Load("diffuse");
        GameObject.FindGameObjectWithTag("img_creatureProgress").GetComponent<Image>().sprite = (Sprite)Resources.Load("green_bar", typeof(Sprite)) as Sprite;

        GameObject.FindGameObjectWithTag("img_monsterProgress").GetComponent<Image>().enabled = true;
        GameObject.FindGameObjectWithTag("img_monsterProgress").GetComponent<Image>().sprite = (Sprite)Resources.Load("green_bar", typeof(Sprite)) as Sprite;
       
    }
    public void Exit()
    {
        Application.Quit();
    }
	// Use this for initialization
	void Start () {
       
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
